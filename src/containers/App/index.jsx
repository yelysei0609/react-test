import React from 'react';

import List from 'components/List';
import Timer from 'components/Timer';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.timer = React.createRef();
  };

  timerStart = () => this.timer.current.start();

  timerPause = () => this.timer.current.pause();
  
  timerReset = () => this.timer.current.reset();

  render() {
    return (
        <div className='container'>
          <div className='controlButtons'>
            <button onClick={this.timerStart}>START TIMER</button>
            <button onClick={this.timerPause}>PAUSE TIMER</button>
            <button onClick={this.timerReset}>RESET TIMER</button>
          </div>

          <List />
          <Timer
            callback={() => console.log('hello')}
            controls
            time={{ min: 0, sec: 12 }}
            ref={this.timer}
          />
        </div>
    );
  };
};

export default App;
