export default function toTwoDigitTimeFormat(time) {
  return time < 10 ? `0${time}` : `${time}`;
}
