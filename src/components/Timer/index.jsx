import React, { Component } from 'react';
import { func, bool, object } from 'prop-types';
import cn from 'classnames';

import { 
  START, 
  PAUSE, 
  RESET, 
  ENDING_TIME 
} from './constants';
import toTwoDigitTimeFormat from 'utils/toTwoDigitTimeFormat';
import './style.scss';

class Timer extends Component {
  state = {
    min: 0,
    sec: 0,
    isStarted: false,
    isRunning: false,
    isOver: false,
    isAlarmed: false
  };

  constructor(props) {
    super(props);

    const { time } = props;

    if (time) {
      const { min, sec } = time;

      this.state.sec = sec;
      this.state.min = min;
    }
  };

  componentWillUnmount() {
    clearInterval(this.intervalId);
  };

  start = () => {
    this.intervalId = setInterval(this.tick, 1000);

    if (this.state.isRunning) {
      return;
    }

    this.setState({ isRunning: true, isStarted: true });
  };

  reset = () => {
    clearInterval(this.intervalId);
    this.setState({
      min: 0,
      sec: 0,
      isStarted: false,
      isRunning: false,
      isOver: false,
      isAlarmed: false
    });
  };

  pause = () => {
    clearInterval(this.intervalId);
    this.setState({ isRunning: false });
  };

  increment = () => this.setState(({ min }) => ({ min: min + 1 }));

  decrement = () => {
    const { min } = this.state;

    if (min === 0) {
      return;
    }

    this.setState({ min: min - 1 });
  };

  tick = () => {
    const { 
      sec,
      min, 
      isStarted, 
      isAlarmed 
    } = this.state;

    if (sec > 0) {
      this.setState({ sec: sec - 1 });
    }

    if (isStarted && sec === 0 && min === 0) {
      this.setState({ isOver: true, isAlarmed: false });
      clearInterval(this.intervalId);
      this.props.callback();

      return;
    }

    if (min < 1 && sec <= ENDING_TIME && !isAlarmed) {
      this.setState({ isAlarmed: true });
    }

    if (sec === 0) {
      this.setState({ min: min - 1, sec: 59 });
    }
  };

  render() {
    const { controls } = this.props;
    
    const { 
      min, 
      sec, 
      isRunning, 
      isOver, 
      isAlarmed 
    } = this.state;

    return (
      <div className={cn('timer', { greenBg: isOver })}>
        <h2 className='timerTitle'>Timer</h2>
        <div className='timerWrapper'>
          <div className='timerMin timerMinWrapper'>
            {controls && !isRunning && (
              <>
                <span
                  className='timerMinArrowUp timerMinArrow'
                  onClick={this.increment}
                />
                <span
                  className='timerMinArrowDown timerMinArrow'
                  onClick={this.decrement}
                />
              </>
            )}
            <span className='timerMin timerTime'>
              {toTwoDigitTimeFormat(min)}
            </span>
          </div>
          <span className='timerSeparator'>:</span>
          <span
            className={cn('timerSec timerTime', {
              timerRedFont: isAlarmed
            })}
          >
            {toTwoDigitTimeFormat(sec)}
          </span>
        </div>

        {controls && (
          <>
            {!isRunning && (
              <button className='timerBtn' onClick={this.start}>
                {START}
              </button>
            )}

            {isRunning && !isOver && (
              <button className='timerBtn' onClick={this.pause}>
                {PAUSE}
              </button>
            )}

            {isOver && (
              <button className='timerBtn' onClick={this.reset}>
                {RESET}
              </button>
            )}
          </>
        )}

        {isOver && <audio autoPlay src='music/horse.mp3'></audio>}
      </div>
    );
  }
}

Timer.propTypes = {
  callback: func,
  controls: bool,
  time: object
};

Timer.defaultProps = {
  callback: () => null
};

export default Timer;
