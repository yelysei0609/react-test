import React, { Component } from 'react';

import './style.scss';
import ListItem from './list.item.component';
import api from '../../api';

class List extends Component {
  state = {
    data: [],
    loading: false,
    selectedItem: ''
  };

  componentDidMount() {
      this.setState({ loading:true });
      api.getList()
        .then(res => {
          this.setState({ loading: false, data: res });
        });
  }

  onClick = name => {
    this.setState({ selectedItem: name });
  };

  render() {
    const { data, loading, selectedItem } = this.state;
    return (
      <ul className='list'>
        {loading ? (
          <h2>Loading...</h2>
        ) : (
          data.map(name => (
            <ListItem
              name={name}
              key={name}
              onClick={this.onClick}
              selected={selectedItem === name}
            />
          ))
        )}
      </ul>
    );
  }
}

export default List;
